/**
  * @file funcs.h
  *
  * declares some basic example math functions
  */

#ifndef _FUNCS_H_
#define _FUNCS_H_

/**
  * Add two integers
  * 
  * @param x The first integer to add.
  * @param y The second integer to add.
  *
  * @return The sum of x and y.
  */

int add(int x, int y);

/**
  * Subtract two integers
  * 
  * @param x The integer being subtracted from
  * @param y The integer that is subtracted

  * @return The difference between x and y.
  */


int sub(int x, int y);

#endif /* _FUNCS_H_ */
