#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include "funcs.h"

int main(int argc, char *argv[])
{
	if (argc != 4) {
		fprintf(stderr, "requires for arguemnts and got %d\n", argc);
		exit(EXIT_FAILURE);
	}
	
	errno = 0;

	int x, y;
	x = strtol(argv[2], NULL, 10);
	y = strtol(argv[3], NULL, 10);

	if (errno != 0) {
		perror("strtol: ");
	}

	if (strcmp(argv[1], "sub") == 0) {
		fprintf(stdout, "%d - %d = %d\n",x, y,  sub(x, y));
	} else if (strcmp(argv[1], "add") == 0) {
		fprintf(stdout, "%d + %d = %d\n",x, y,  add(x, y));
	} else {
		fprintf(stderr, "%s is not a valid math.\n", argv[2]);
		exit(EXIT_FAILURE);
	}

	return EXIT_SUCCESS;
}
