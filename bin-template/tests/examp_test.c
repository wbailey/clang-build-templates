#include <criterion/criterion.h>
#include <stdio.h>
#include <stdlib.h>

TestSuite(examp_test);

Test(examp_test, add)
{
	FILE *output = popen("./bin/examp add 10 10", "r");
	char *expected = "10 + 10 = 20";
	size_t len = strlen(expected) + 1;
	char *buffer = malloc(sizeof(char) * len);
	fgets(buffer, len, output);
	int exit_code = pclose(output);

	cr_expect(exit_code == 0, "exit code should be 0 but got %d", exit_code);
	cr_expect(strcmp(buffer, expected)  == 0, 
			"output should be %s but got %s", expected,  buffer);
}

Test(examp_test, sub)
{
	FILE *output = popen("./bin/examp sub 10 10", "r");
	char *expected = "10 - 10 = 0";
	size_t len = strlen(expected) + 1;
	char *buffer = malloc(sizeof(char) * len); 
	fgets(buffer, len, output);
	int exit_code = pclose(output);

	cr_expect(exit_code == 0, "exit code should be 0 but got %d", exit_code);
	cr_expect(strcmp(buffer, expected)  == 0, 
			"output should be %s but got %s", expected, buffer);
}

Test(examp_test, bad_input)
{
	FILE *output = popen("./bin/examp taco 10 10", "r");
	int exit_code = pclose(output);

	cr_expect(exit_code != 0, "exit code should not be 0 but got", exit_code);
}
