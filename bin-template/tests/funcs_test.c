#include <criterion/criterion.h>
#include <stdio.h>
#include "../src/funcs.h"

int out_num = 10;

void setup(void)
{
	out_num++;
	// printf("setting up...\n");
}

void teardown(void)
{
	// printf("tearingdown...\n");
}

TestSuite(funcs_test, .init = setup, .fini = teardown);

Test(funcs_test, add) {
	int num = add(10, 15);
	cr_expect(num == 25, "num should equal 25");
	cr_expect(out_num == 11, "out_num should equal 11 and it is %d", out_num);
}

Test(funcs_test, sub) {
	int num = sub(10, 15);
	cr_expect(num == -5, "num should equal -5 and num is %d", num);
}

