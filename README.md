# C Languages Build Templates 🏗️

C language project/build templates that use TDD ([criterion](https://github.com/Snaipe/Criterion)), [GNU make](https://www.gnu.org/software/make/) and [Doxygen](https://github.com/doxygen/doxygen).

* [bin-template](https://gitlab.com/wbailey/clang-build-templates/-/tree/main/bin-template) is for single entry point binaries.
* [lib-template](https://gitlab.com/wbailey/clang-build-templates/-/tree/main/lib-template) is for libraries.

## Usage

Configure the binary/library name in the makefile using the `MAIN` variable:

```make
MAIN = examp
```

These templates compile the source code from `src` to an executable in the `bin` directory or a library in the `lib` directory, and run the tests found in `tests`.

The directory structure is as follows:

```shell
 ├── bin/lib				# contains the binary build (created by make)
 ├── obj					# contians object files used to build (created by make)
 ├── src					# source code goes here
 └── tests					# place unit tests here, with .c extension
     └── bin				# contains binaries of unit tests (created by make)
```

Directories can be configured in the makefile:

```make
SRC_DIR = src
OBJ_DIR = obj
BIN_DIR = bin
LIB_DIR = lib
TEST_DIR = tests
```

The `README.md` in each directory is configured as the front page of the docs, and the title of the docs can be updated in the `Doxyfile`, under the `PROJECT_NAME` key. 

### Building

* `$ make` - builds a with debug flags and runs all tests
* `$ make relaese` - builds without debug flags, runs all tests
* `$ make test` - runs tests only, rebuilds as needed
* `$ make test-clean` - removes test binaries
* `$ make docs` - builds docs from doxygen comments
* `$ make docs-clean` - removes docs
* `$ make clean` - removes build objects and binaries
* `$ make submit` - (WIP) zips binary

### CLI

#### Testing

* `./tests/bin/<test_name>` - run a test individually
* `./tests/bin/<test_name> --verbose` - run a test with verbose output
* `./tests/bin/<test_name> -h` - Criterion help

## Writing Tests

TODO: add Criterion cheatsheet

## Writing Docs

TODO: add Doxygen Cheatsheet

## Todos

* update readme
	* [x] include descriptions of make rules
	* [x] inlcude details of each template
	* [ ] include info/links on criterion
	* [ ] include info/links on doxygen
	* [x] include discussion of building/commands
* [x] fix the pre and post test functions that are not working
* add dbg.h
* [x] add testing to main entrypoint in bin, passing in cli args
* [x] add doxygen
* [ ] fix submit rule
* [x] `$(TEST_DIR)/bin` might make sense to refer to the `$(BIN_DIR)` in both cases
* [ ] valgrind perf testing/test output might be good
* [ ] maybe add a valgrind/gdb cheatsheet
